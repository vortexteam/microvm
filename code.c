#include "constants.h"

int code[] = {

// create return stack
set, eax, 1024, 0,
alc, eax, rrs, 0,
set, eax, 0, 0,
sv, eax, rrs, 0,

// create stack
set, eax, 1024, 0,
alc, eax, rs, 0,
set, eax, 0, 0,
sv, eax, rs, 0,

RESOLVE MAIN, ehx,
jmp, ehx, 0, 0,

// call proc (hx)
LABEL CALL_PROC,
ld,  rrs, eax, 0,
inc, eax, 0, 0,
sv,  eax, rrs, 0,
set, hax, 0, 0,
set8, rbx, 4, 0,
mlt8, rax, rbx, rcx,
add8, rrs, rcx, rbx,
sv, egx, rbx, 0,
jmp, ehx, 0, 0,

// return proc
LABEL RET,
ld,  rrs, eax, 0,
set, hax, 0, 0,
set8, rbx, 4, 0,
mlt8, rax, rbx, rcx,
add8, rrs, rcx, rbx,
ld, rbx, ecx, 0,
dec, eax, 0, 0,
sv, eax, rrs, 0,
jmp, ecx, 0, 0,

// push edx to stack
LABEL PUSH, 
ld, rs, eax, 0,
inc, eax, 0, 0,
sv, eax, rs, 0,
set, hax, 0, 0,
set8, rbx, 4, 0,
mlt8, rax, rbx, rcx,
add8, rs, rcx, rbx,
sv, edx, rbx, 0,
RETURN

// pop from stack to edx
LABEL POP,
ld, rs, eax, 0,
set, hax, 0, 0,
set8, rbx, 4, 0,
mlt8, rax, rbx, rcx,
add8, rs, rcx, rbx,
ld, rbx, edx, 0,
dec, eax, 0, 0,
sv, eax, rs, 0,
RETURN

// sqr
LABEL SQR,
CALL(POP)
mlt, edx, edx, edx,
CALL(PUSH)
RETURN

// factorial
LABEL FAC,
CALL(POP)
RESOLVE FAC1, eax,
jz, eax, edx, 0,
CALL(PUSH)
set, eex, 1, 0,
sub, edx, eex, edx,
CALL(PUSH)
CALL(FAC)
CALL(POP)
prnt, edx, 0, 0,
mov, edx, efx, 0,
CALL(POP)
mlt, edx, efx, edx,
CALL(PUSH)
RETURN
LABEL FAC1,
set, edx, 1, 0,
CALL(PUSH)
RETURN

LABEL MAIN, 
set, edx, 10, 0,
CALL(PUSH)
CALL(FAC)
CALL(POP)
prnt, edx, 0, 0,

EXIT

};


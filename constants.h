#define PROGRAM_END 0
#define CODE_VERSION  0

#define i8 int8_t
#define i32 int32_t
#define i64 int64_t

#define LABEL_CODE 8888
#define LABEL LABEL_CODE, 0, 0, 
#define RESOLVE rslv, 0,
#define CALL(X) mov8, rip, rgx, 0, set8, rhx, 20, 0, add8, rgx, rhx, rgx, RESOLVE X, ehx, RESOLVE CALL_PROC, hhx, jmp, hhx, 0, 0,
#define RETURN RESOLVE RET, ehx, jmp, ehx, 0, 0,
#define EXIT PROGRAM_END, 0, 0, 0

int procs[100];

// Registers
// 64-bit
#define rax 0
#define rbx 1
#define rcx 2
#define rdx 3
#define rex 4
#define rfx 5
#define rgx 6
#define rhx 7
// instruction pointer
#define rip 8
// return stack address
#define rrs 9
// stack address
#define rs 10

// 32-bit
#define eax 0
#define hax 1
#define ebx 2
#define hbx 3
#define ecx 4
#define hcx 5
#define edx 6
#define hdx 7
#define eex 8
#define hex 9
#define efx 10
#define hfx 11
#define egx 12
#define hgx 13
#define ehx 14
#define hhx 15
#define ip 16

// 8-bit
#define ax 0
#define bx 8
#define cx 16
#define dx 24
#define ex 32
#define fx 40
#define gx 48
#define hx 56

// Instructions
// copy values between registers
#define mov 1
#define mov1 2
#define mov8 3

// set value
#define set 8
#define set1 9
#define set8 10

#define MEM_INS_OFF 20
// load from memory
#define ld  (MEM_INS_OFF + 0)
#define ld1 (MEM_INS_OFF + 1)
#define ld8 (MEM_INS_OFF + 2)
// save to memory
#define sv  (MEM_INS_OFF + 5)  
#define sv1 (MEM_INS_OFF + 6)  
#define sv8 (MEM_INS_OFF + 7)  
// allocate
#define alc (MEM_INS_OFF + 10)  
// free
#define fr  (MEM_INS_OFF + 11)  

#define MATH_INS_OFF (MEM_INS_OFF + 20)
// increment
#define inc (MATH_INS_OFF + 0 )
#define inc1 (MATH_INS_OFF + 1)
#define inc8 (MATH_INS_OFF + 2)
// decrement
#define dec (MATH_INS_OFF + 5 )
#define dec1 (MATH_INS_OFF + 6)
#define dec8 (MATH_INS_OFF + 7)

// add
#define add (MATH_INS_OFF + 10)
#define add1 (MATH_INS_OFF + 11)
#define add8 (MATH_INS_OFF + 12)

// multiply
#define mlt (MATH_INS_OFF + 15)
#define mlt1 (MATH_INS_OFF + 16)
#define mlt8 (MATH_INS_OFF + 17)

// subtract
#define sub (MATH_INS_OFF + 20)
#define sub1 (MATH_INS_OFF + 21)
#define sub8 (MATH_INS_OFF + 22)

// print
#define prnt 5555
#define prnt1 5556
#define prnt8 5557

#define JMP_INS_OFF (MATH_INS_OFF + 100)
// jump
#define jmp (JMP_INS_OFF + 0)
#define rslv (JMP_INS_OFF + 1)
#define jz (JMP_INS_OFF + 2)

// procedures
#define MAIN 0
#define RET 1
#define CALL_PROC 2
#define PUSH 3
#define POP 4
#define SQR 5
#define FAC 6
#define FAC1 7

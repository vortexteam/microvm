#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "code.c"

struct Vm {
    i32* mem;
    i64* mem8;
    i8* mem1;
    int* code;
    int cp;
};

typedef struct Vm Vm;

int checkSize(int size, int expected, char* errMsg) {
    if (size != expected) {
        printf(errMsg, size);
        return 1;
    } else
        return 0;
}

void* newVm() {
    Vm* vm = malloc(sizeof(struct Vm));
    vm->mem = malloc(320);
    vm->mem1 = (i8*) vm->mem;
    vm->mem8 = (i64*) vm->mem;
    vm->code = malloc(65536);
    vm->cp = 0;
    return vm;
}

void freeVm(Vm* vm) {
    free(vm->mem);
    free(vm->code);
    free(vm);
}

void addInstruction(Vm* vm, int ins, int arg0, int arg1, int arg2) {
    vm->code[vm->cp++] = ins;
    vm->code[vm->cp++] = arg0;
    vm->code[vm->cp++] = arg1;
    vm->code[vm->cp++] = arg2;
}

int gn(Vm* vm) {
    int res = vm->code[vm->mem[ip]];
    vm->mem[ip]++;
    return res;
}

// Instructions implementation.
void inoop(Vm* vm, int arg0, int arg1, int arg2) {
}

void imov(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem[arg1] = vm->mem[arg0];
}

void imov1(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem1[arg1] = vm->mem1[arg0];
}

void imov8(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem8[arg1] = vm->mem8[arg0];
}

void iset(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem[arg0] = arg1;
}

void iset1(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem1[arg0] = arg1;
}

void iset8(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem8[arg0] = arg1;
}

void ild(Vm* vm, int arg0, int arg1, int arg2) {
    //printf("load addr=%I64d val=%d\n", (i64)vm->mem8[arg0], *((i32*)(vm->mem8[arg0])));
    vm->mem[arg1] = *((i32*)(vm->mem8[arg0]));
}

void ild1(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem1[arg1] = *((i8*)(vm->mem8[arg0]));
}

void ild8(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem8[arg1] = *((i64*)(vm->mem8[arg0]));
}

void isv(Vm* vm, int arg0, int arg1, int arg2) {
    //printf("save addr=%I64d val=%d\n", (i64)vm->mem8[arg1], vm->mem[arg0]);
    *((i32*)(vm->mem8[arg1])) = vm->mem[arg0];
}

void isv1(Vm* vm, int arg0, int arg1, int arg2) {
    *((i8*)(vm->mem8[arg1])) = vm->mem1[arg0];
}

void isv8(Vm* vm, int arg0, int arg1, int arg2) {
    *((i64*)(vm->mem8[arg1])) = vm->mem8[arg0];
}

void ialc(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem8[arg1] = (i64) malloc(vm->mem[arg0]);
}

void ifr(Vm* vm, int arg0, int arg1, int arg2) {
    free((void*) vm->mem8[arg0]);
}

void iinc(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem[arg0]++;
}

void iinc1(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem1[arg0]++;
}

void iinc8(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem8[arg0]++;
}

void idec(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem[arg0]--;
}

void idec1(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem1[arg0]--;
}

void idec8(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem8[arg0]--;
}

void iadd(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem[arg2] = vm->mem[arg0] + vm->mem[arg1];
}

void iadd1(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem1[arg2] = vm->mem1[arg0] + vm->mem1[arg1];
}

void iadd8(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem8[arg2] = vm->mem8[arg0] + vm->mem8[arg1];
}

void imlt(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem[arg2] = vm->mem[arg0] * vm->mem[arg1];
}

void imlt1(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem1[arg2] = vm->mem1[arg0] * vm->mem1[arg1];
}

void imlt8(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem8[arg2] = vm->mem8[arg0] * vm->mem8[arg1];
}

void isub(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem[arg2] = vm->mem[arg0] - vm->mem[arg1];
}

void isub1(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem1[arg2] = vm->mem1[arg0] - vm->mem1[arg1];
}

void isub8(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem8[arg2] = vm->mem8[arg0] - vm->mem8[arg1];
}

void ijmp(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem[ip] = vm->mem[arg0];
}

void irslv(Vm* vm, int arg0, int arg1, int arg2) {
    vm->mem[arg2] = procs[arg1];
}

void ijz(Vm* vm, int arg0, int arg1, int arg2) {
    if (vm->mem[arg1] == 0)
        vm->mem[ip] = vm->mem[arg0];
}

void iprnt(Vm* vm, int arg0, int arg1, int arg2) {
    printf("%d\n", vm->mem[arg0]);
}

void iprnt1(Vm* vm, int arg0, int arg1, int arg2) {
    printf("%c\n", vm->mem1[arg0]);
}

void iprnt8(Vm* vm, int arg0, int arg1, int arg2) {
    printf("%I64d\n", vm->mem8[arg0]);
}

// Execution
void eins(Vm* vm, int instr) {
    void (*instrFn)(Vm*, int, int, int);
    instrFn = &inoop;
    switch (instr) {
        case mov:  instrFn = &imov; break;
        case mov1: instrFn = &imov1; break;
        case mov8: instrFn = &imov8; break;
        case set:  instrFn = &iset; break;
        case set1: instrFn = &iset1; break;
        case set8: instrFn = &iset8; break;
        case ld:  instrFn = &ild; break;
        case ld1: instrFn = &ild1; break;
        case ld8: instrFn = &ild8; break;
        case sv:  instrFn = &isv; break;
        case sv1: instrFn = &isv1; break;
        case sv8: instrFn = &isv8; break;
        case alc: instrFn = &ialc; break;
        case fr:  instrFn = &ifr; break;
        case inc:  instrFn = &iinc; break;
        case inc1: instrFn = &iinc1; break;
        case inc8: instrFn = &iinc8; break;
        case dec:  instrFn = &idec; break;
        case dec1: instrFn = &idec1; break;
        case dec8: instrFn = &idec8; break;
        case add:  instrFn = &iadd; break;
        case add1: instrFn = &iadd1; break;
        case add8: instrFn = &iadd8; break;
        case mlt:  instrFn = &imlt; break;
        case mlt1: instrFn = &imlt1; break;
        case mlt8: instrFn = &imlt8; break;
        case sub:  instrFn = &isub; break;
        case sub1: instrFn = &isub1; break;
        case sub8: instrFn = &isub8; break;
        case prnt:  instrFn = &iprnt; break;
        case prnt1: instrFn = &iprnt1; break;
        case prnt8: instrFn = &iprnt8; break;
        case jmp:  instrFn = &ijmp; break;
        case rslv:  instrFn = &irslv; break;
        case jz:  instrFn = &ijz; break;
    }
    int arg0 = gn(vm);
    int arg1 = gn(vm);
    int arg2 = gn(vm);
    //printf("exec ip=%d, instr=%d, arg0=%d, arg1=%d, arg2=%d\n", vm->mem[ip] - 4, instr, arg0, arg1, arg2);
    instrFn(vm, arg0, arg1, arg2);
}

void exec(Vm* vm, int addr) {
    vm->mem[ip] = addr;
    while (1 == 1) {
        int instr = gn(vm);
        if (instr == PROGRAM_END)
            break;
        eins(vm, instr);
    }
}

void fillCode(Vm* vm) {
    int i = 0;
    while (1 == 1) {
        int s = i * 4;
        //printf("%d : fill instr=%d, arg0=%d, arg1=%d, arg2=%d\n", s, code[s], code[s+1], code[s+2], code[s+3]);
        if (code[s] == LABEL_CODE) {
            procs[code[s + 3]] = (i + 1) * 4;
            addInstruction(vm, -1, 0, 0, 0);
            //printf("label %d is on addr %d\n", code[s + 3], (i + 1) * 4);
        } else {
            addInstruction(vm, code[s], code[s + 1], code[s + 2], code[s + 3]);
            if (code[s] == PROGRAM_END) {
                break;
            }
        }
        i++;
    }
}

void main() {
    int errS = 0;
    errS += checkSize(sizeof(char),      1, "System is unsupported, char size is %d");
    errS += checkSize(sizeof(short),     2, "System is unsupported, short size is %d");
    errS += checkSize(sizeof(int),       4, "System is unsupported, int size is %d");
    errS += checkSize(sizeof(long long), 8, "System is unsupported, long long size is %d");
    errS += checkSize(sizeof(float),     4, "System is unsupported, float size is %d");
    errS += checkSize(sizeof(double),    8, "System is unsupported, double size is %d");
    errS += checkSize(sizeof(void*),     8, "System is unsupported, void* size is %d");

    if (errS > 0) 
        return;

    Vm* vm = newVm();

    if (vm == 0)
        return;

    fillCode(vm);
    exec(vm, 0);
    freeVm(vm);
}